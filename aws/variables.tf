
variable "vm_list" {
type = list(string)
default = ["VM1","VM2"]
}
variable "private_ips" {
type = list(list(string))
default = [["192.168.0.4"],["192.168.0.5"]]
}
variable "subnet_list" {
type = list(string)
default = ["east-sub","west-sub","nord-sub","south-sub"]
}
variable "subnet_cidr_list" {
type = list(string)
default = ["192.168.0.0/20","192.168.16.0/20","192.168.32.0/20","192.168.48.0/20"]
}
variable "ami" {
default = "ami-0d382e80be7ffdae5"
}

