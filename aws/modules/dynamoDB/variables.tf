variable "name" {

}
variable billing_mode {
default ="PROVISIONED"
}
variable "read_capacity" {
 default = 20
}
variable "write_capacity" {
 default = 20
}
variable "hash_key" {
default = "ID"
}
variable "range_key" {

}
variable "range_key_type" {
default = "S"
}
variable "hash_key_type" {
default = "S"
}

