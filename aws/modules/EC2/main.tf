
resource "aws_instance" "cerberus" {
     ami = var.ami
     instance_type = var.instance_type
     key_name = var.keypair
     user_data = file ("script.sh")
  #  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
     

      
  network_interface {
    network_interface_id = aws_network_interface.interface[count.index].id
    device_index         = var.ni_device_index
                       }
 
     tags = {
    Name = var.vm_list[count.index]
            }
    
count = length (var.vm_list) 
}

resource "aws_eip" "lb" {
 # instance = aws_instance.cerberus[0].id
  vpc      = true
  network_interface = aws_network_interface.interface[count.index].id

count = length (var.vm_list)
}

resource "aws_internet_gateway" "gw" {
  vpc_id = var.vpc_id

  tags = {
    Name = "my-gw"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = var.security_groupe_name
  description = "Allow SSH inbound traffic"
  vpc_id = var.vpc_id 
  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_network_interface" "interface" {
  subnet_id   = var.subnet_id
  private_ips = var.private_ips[count.index]
  security_groups = [aws_security_group.allow_ssh.id] 
  tags = {
    Name = var.network_interface_name
  }
count = length (var.vm_list)
}
