variable "keypair" {

}
variable instance_type {
default = "t2.micro"
}
variable "ami" {
default = "ami-0fe4e60d1aad1f54b"
}
variable "vm_list" {
type = list(string)

}
variable "security_groupe_name" {
default = "allow_ssh"
}
variable subnet_id {}
variable vpc_id {}
variable network_interface_name {
default = "primary_network_interface"
}
variable private_ips {
type = list(list(string))
}
variable "ni_device_index" {
default = 0 
}

