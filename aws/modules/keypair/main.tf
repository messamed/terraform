
resource "aws_key_pair" "cerberus-key" {
  key_name   = var.key_name
  public_key = file(var.pubkey_path)
}

output "keypair_id" {
value = aws_key_pair.cerberus-key.id
}
