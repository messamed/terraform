resource "aws_vpc" "main" {
  cidr_block       = var.cidr_block
  instance_tenancy = var.instance_tenancy

  tags = {
    Name = var.name
  }
}
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_cidr_list[count.index]
  map_public_ip_on_launch = true 
  tags = {
    Name = var.subnet_list[count.index]
  }
count = length(var.subnet_list)
}

