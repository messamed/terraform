
module "EC2" {
source = "./modules/EC2/"

ami = var.ami
vm_list = var.vm_list
keypair = module.keypair.keypair_id
vpc_id = module.vpc.vpc_id 
subnet_id   = module.vpc.subnet_ids[0]
private_ips = var.private_ips
} 
module "keypair" {
source = "./modules/keypair/"
pubkey_path = "ssh/id_rsa.pub"

}
module "S3" {
 source = "./modules/S3/"
 bucketName = "mybucket-9832"
 key = "ssh public key"
 contentPath = "ssh/id_rsa.pub"
}
module "dynamoDB" {
 source = "./modules/dynamoDB/"
 name = "simpleTable"
 range_key= "range"

}

module "vpc" {
 source = "./modules/vpc/"
 name = "my_vpc"
 cidr_block = "192.168.0.0/16"
 subnet_list = var.subnet_list
 subnet_cidr_list = var.subnet_cidr_list
}
