resource "openstack_compute_instance_v2" "myinstance" {
  name            = var.name
  image_name        = var.image_name
  flavor_name       = var.flavor_name
  security_groups = ["default"]
  key_pair        = openstack_compute_keypair_v2.my-keypair.id
  network {
    name = openstack_networking_network_v2.network_1.name
  }
}

resource "openstack_compute_keypair_v2" "my-keypair" {
  name = "my-keypair"
  public_key = file("ssh/id_rsa.pub")
}

