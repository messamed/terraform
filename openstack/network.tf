resource "openstack_networking_network_v2" "network_1" {
  name           = "my-network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_1" {
  name            = "my-subnet"
  network_id      = openstack_networking_network_v2.network_1.id
  cidr            = var.local-network[0].ip_rang
  dns_nameservers = var.local-network[0].dns_nameservers
  ip_version      = 4
  allocation_pool {
    start         = var.local-network[0].allocation_pool[0]
    end           = var.local-network[0].allocation_pool[1]
  }
}


