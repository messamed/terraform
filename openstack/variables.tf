variable "name" {
default = "myinstance"
}
variable "flavor_name" {
default = "medium"
}
variable "image_name" {
default = "centos-7"
}



variable "local-network" {
  type = list(object({
    ip_rang = string
    dns_nameservers = list(string)
    allocation_pool = list(string)
  }))
  default = [
    {
      ip_rang = "10.0.0.0/24"
      dns_nameservers = ["172.16.31.1", "8.8.8.8"]
      allocation_pool = ["10.0.0.10", "10.0.0.254"]
    }
  ]
}


variable "floating-vpn-pool" {
  type = map
  default = {
      name = "external-vpn"
      id = "f3f40e95-e011-4383-b776-2dbd9da48de8"
    }
}

