module "windowsservers" {
  source              = "Azure/compute/azurerm"
  resource_group_name = azurerm_resource_group.example.name
  is_windows_image    = true
  vm_hostname         = "mywinvm" // line can be removed if only one VM module per resource group
  
  admin_password      = "ComplxP@ssw0rd!"
  vm_os_simple        = "WindowsServer"
  // change to a unique name per datacenter region
  vnet_subnet_id      = module.network.vnet_subnets[0]
}
