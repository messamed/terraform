
module "linuxservers" {
  source              = "Azure/compute/azurerm"
  resource_group_name = "example-resources"
  vm_os_simple        = "UbuntuServer"
  public_ip_dns       = ["linsimplevmips"] // change to a unique name per datacenter region
  vnet_subnet_id      = module.network.vnet_subnets[0]
  ssh_key = "ssh/id_rsa.pub"

   depends_on = [azurerm_resource_group.example]

}


